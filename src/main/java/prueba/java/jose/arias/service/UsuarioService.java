package prueba.java.jose.arias.service;


import prueba.java.jose.arias.model.Usuario;
import prueba.java.jose.arias.model.UsuarioDto;
import prueba.java.jose.arias.repository.UsuarioRepository;
import prueba.java.jose.arias.utils.PasswordUtils;
import java.io.Serializable;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jose.Arias
 */
@Service
public class UsuarioService implements Serializable {

    private static final long serialVersionUID = 1L;
    private UsuarioDto usuarioDto;

    @Autowired
    private UsuarioRepository repository;

    public UsuarioDto save(Usuario usuario) {
        String senhaEncoded = PasswordUtils.gerarBCrypt(usuario.getPassword());
        
        Usuario newUsuario = new Usuario(usuario.getName(), usuario.getEmail(), senhaEncoded, usuario.getPhones());
        
        Usuario user = repository.save(newUsuario);
        
        Usuario userUpdate = createToken(user);
         usuarioDto = new UsuarioDto(userUpdate.getId(), userUpdate.getCreated(), 
                                     userUpdate.getModified(), userUpdate.getLast_login(), userUpdate.getToken());
         
         
        return usuarioDto;      
    }

    public Usuario fyndByEmail(String email) {
        return repository.findByEmail(email);
    }

    public Usuario createToken(Usuario usuario) {        
        String senhaEncoded = PasswordUtils.gerarBCrypt(usuario.getId().toString());
        usuario.setToken(senhaEncoded);
        Usuario user = repository.save(usuario);
        return user;
    }

    public Usuario validaToken(String token) {        
        return repository.findByToken(token);
    }
    public Usuario findByUUID(UUID id) {        
        return repository.findById(id);
    }
    
    
    
    
}
