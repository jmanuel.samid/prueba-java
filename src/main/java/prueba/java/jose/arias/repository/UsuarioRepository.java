package prueba.java.jose.arias.repository;


import prueba.java.jose.arias.model.Usuario;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jose.Arias
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    
    Usuario findByName(String name);
    Usuario findByEmail(String email);
    Usuario findById(UUID id);
    Usuario findByToken(String token);
            
} 
    

