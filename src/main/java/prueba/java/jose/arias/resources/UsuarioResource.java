package prueba.java.jose.arias.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import prueba.java.jose.arias.response.Response;
import prueba.java.jose.arias.service.UsuarioService;
import prueba.java.jose.arias.model.Usuario;
import prueba.java.jose.arias.model.UsuarioDto;
import prueba.java.jose.arias.utils.PasswordUtils;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jose.Arias
 */
@RequestMapping(value = "api/v1/")
@RestController
public class UsuarioResource {

    private final Logger logger = LoggerFactory.getLogger(UsuarioResource.class);
    private UsuarioDto usuarioDto;
    
    @Autowired
    UsuarioService service;

    /**
     * POST / : Create usuario.
     *
     * body:{ "name": value 
     *        "email": value 
     *        "password": value 
     *        "phones":[ {
     *                    "number": value 
     *                    "ddd": value } 
     *                 ] 
     *      }
     * @param {Usuario} - Usuario object
     *
     * @return 200 - OK and Usuario created
     * @return 400 - E-mail already exist
     */
    @PostMapping(value = "create")
    public ResponseEntity add(@RequestBody Usuario usuario) {
        Response response = new Response();
        logger.debug("REST request to save Tool : {}", usuario);

        if (usuario.getName().isEmpty() || usuario.getPassword().isEmpty()) {
            response.setMensagem("name and password cant be empty!");
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
        }

        if (!Usuario.checkEmail(usuario.getEmail())) {
            response.setMensagem("put a valid mail!");
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
        }

        if (service.fyndByEmail(usuario.getEmail()) != null) {
            response.setMensagem("E-mail already exist");
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.status(HttpStatus.OK).body(service.save(usuario));

    }

    /**
     * POST / : check usuario.
     *
     * body:{ "email": value "password": value }
     *
     * body:{ "email": value
     *        "senha": value
     *      }
     *
     * @return 200 - OK
     * @return 401 - Invalid user and or password
     * @return 401 - Invalid user and or password
     *
     */
    @PostMapping(value = "access")
    public ResponseEntity access(@RequestBody Usuario user) {

        logger.debug("REST request validate access : {}", user);
        Usuario usuario = service.fyndByEmail(user.getEmail());
        Response response = new Response();
        if (usuario != null && (PasswordUtils.passwordValida(user.getPassword(), usuario.getPassword())) == true) {

            usuarioDto = new UsuarioDto(usuario.getId(), usuario.getCreated(),
                    usuario.getModified(), usuario.getLast_login(), usuario.getToken());

            return ResponseEntity.ok().body(usuarioDto);
        } else {
            response.setMensagem("username and password are invalid!");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }

    }

    /**
     * POST / : check token.
     *
     * body:{ "token": value}
     *
     * @param {id} - Id Usuario
     * 
     * @return 200 - OK
     * @return 401 - Invalid user and or password
     * @return 401 - Invalid user and or password
     * @return 401 - Sessão inválida
     * 
     *
     */
    @PostMapping(value = "token")
    public ResponseEntity token(@RequestHeader(value = "token") String token, @RequestParam String id) {

        logger.debug("REST request validate access : {}", token);
        Response response = new Response();

        if (token.isEmpty() || id.isEmpty()) {
            response.setMensagem("id and token cant be empty");
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
        }

        Usuario user = service.validaToken(token);

        if (user == null) {
            response.setMensagem("no authorization");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }

        UUID uuid = UUID.fromString(id);

        Usuario userUUid = service.findByUUID(uuid);

        if (userUUid == null) {
            response.setMensagem("no authorization");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }

        if (userUUid.getToken().equals(token)) {

            LocalDateTime dataNow = LocalDateTime.now();
            LocalDateTime lastLogin = userUUid.getLast_login();
            Duration duracao = Duration.between(dataNow, lastLogin);

            if (duracao.toMinutes() > 30) {
                response.setMensagem("Invalid Password");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
            } else {

                usuarioDto = new UsuarioDto(userUUid.getId(), userUUid.getCreated(),
                        userUUid.getModified(), userUUid.getLast_login(), userUUid.getToken());
                return ResponseEntity.ok().body(usuarioDto);
            }

        } else {
            response.setMensagem("no authorization");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);

        }
    }

}
