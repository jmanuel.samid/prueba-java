package prueba.java.jose.arias.response;

import java.io.Serializable;

/**
 *
 * @author Jose.Arias
 */
public class Response<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private T mensagem;

    public Response() {
    }

    public T getMensagem() {
        return mensagem;
    }

    public void setMensagem(T mensagem) {
        this.mensagem = mensagem;
    }
}
