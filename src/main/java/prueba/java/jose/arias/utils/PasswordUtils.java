package prueba.java.jose.arias.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
/**
 *
 * @author Andrade.Sampaio
 */
public class PasswordUtils {
    /**
     * Gera um hash utilizando o BCrypt.
     *
     * @param password
     * @return String
     */
    public static String gerarBCrypt(String password) {
        if (password == null) {
            return password;
        }
        BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
        return bCryptEncoder.encode(password);
    }

    /**
     * verify and valid the password.
     *
     * @param password
     * @param passwordEncoded
     * @return boolean
     */
    public static boolean passwordValida(String password, String passwordEncoded) {
        BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
        return bCryptEncoder.matches(password, passwordEncoded);
    }

}
