package prueba.java.jose.arias;

import prueba.java.jose.arias.model.Phone;
import prueba.java.jose.arias.model.Usuario;
import prueba.java.jose.arias.model.UsuarioDto;
import prueba.java.jose.arias.service.UsuarioService;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PruebajavaApplicationTests {

    @Autowired
    private UsuarioService service;
    private static Usuario usuarioTeste;
    private static UsuarioDto usuarioDto;

    @Test
    public void testSave() {
        Phone dados = new Phone("945271201", "56");
        List<Phone> listPhone = new ArrayList<Phone>();
        listPhone.add(dados);

        Usuario newUsuario = new Usuario("Jose Arias", "jose.arias@dominio.cl", "12456", listPhone);
        usuarioDto = service.save(newUsuario);
        Assert.assertNotNull("registros encontrados = 0", usuarioDto);

    }

    @Test
    public void testFindEmail() {

        Usuario usuarioSaved = service.findByUUID(usuarioDto.getId());
        Assert.assertTrue("records not found in search", usuarioSaved.getEmail().equals("jose.arias@dominio.cl"));
    }

    @Test
    public void testPassWord() {

        Usuario user = new Usuario("jose.arias@dominio.cl", "12456");
        Usuario usuario = service.fyndByEmail(user.getEmail());
        Assert.assertTrue("Login Success",
                usuario != null && usuario.getEmail().equals(user.getEmail()));
    }

    @Test
    public void checkEmail() {
        Usuario user = new Usuario("jose.arias@dominio.cl", "12456");
        boolean status = Usuario.checkEmail(user.getEmail());
        Assert.assertTrue("Invalid Email", status);
    }
    
    
    @Test
    public void checkToken() {       
        Assert.assertTrue("Invalid Token", usuarioDto.getToken().equals(usuarioDto.getToken()));
    }

}
